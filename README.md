# Configure CQL evaluation engine and HAPI FHIR server to use FHIR resources

## Configure CQL evaluation engine and HAPI FHIR server
We've already discussed how to dockerize CQL evaluation engine (see at https://bitbucket.org/rezacsedu/cql_dockerize/src/master/) and HAPI FHIR server (see at https://bitbucket.org/rezacsedu/hapi_fhir_dockerize/src/master/) as two seperate services to be exposed on the same or different server/machine but on different ports.
However, to make up and running two services, we've prepared a `docker-compose.yml` file (credit goes to Joao Bosco Jares), which can be used to start both services as follows: 

    Step 1: $ `sudo git clone https://rezacsedu@bitbucket.org/rezacsedu/sample_fhir_resources.git`
    Step 2: $ `cd sample_fhir_resources`
    Step 3: $ `sudo docker-compose up -d` # if you want them up and running foreground, remove flag `-d`. 

By default, HAPI FHIR server will be exposed on 8080 and CQL evaluation engine on port 8082. However, if you want, change the port number in the `docker-compose.yml` file. 

## Data preparation
We generate synthetic data of about 66 patients using Synthea tool (see https://synthetichealth.github.io/synthea/). This enables us using the data without concern of legal or privacy restrictions. Patient records are based on a set of de-identified data recommended by the clinicians and real-world statistics collected by the CDC, NIH, and other sources. 

Each patient is simulated independently from birth to present day and their diseases, conditions and medical care describing a progression of states and the transitions between them. Thus, for each synthetic patient, the data contains a complete medical history, including medications, medical encounters, and social determinants of health. 
We also introduced minor biases to patient observations -e.g. weight, height, blood pressure, heart rate etc. knowing the fact that patient would have a slightly different observation (e.g. for a certain condition) in subsequent visits to hospitals without changing the patient ID. 

The condition and observation encoded with LOINC (https://loinc.org/) and SNOMED(https://www.snomed.org/), respectively. We convert patient data into a set of FHIR resources bundles based on observation and condition value sets and distribute them into three DICs to simulate different hospitals. 
The sample data is upload in this repository see at https://bitbucket.org/rezacsedu/sample_fhir_resources/src/master/out1.tar. 
 
## Metadata preparation 
Metadata scheme is based on the condition (see https://www.hl7.org/fhir/condition-examples.html) and observation (https://www.hl7.org/fhir/observation-examples.html)resource's examples provided by FHIR, the basic metadata information is collected by looking up corresponding examples in (Meaningful Use Value Sets) from the United States Health Information Knowledge Base (see https://ushik.ahrq.gov/ValueSets?system=mu). 
The schema is first extracted inspired by one of our previous publications. Then the resulting schema was further encoded into FHIR standard and publicly exposed using a dedicated FHIR server. 

The sample metadata for the terminology server is upload in this repository see at https://bitbucket.org/rezacsedu/sample_fhir_resources/src/master/resources.tar. 

## Downloading data
To download and use these FHIR resources, just clone this repo and unzip the files on your machine as follows:

    Step 1: $ `sudo git clone https://bitbucket.org/rezacsedu/sample_fhir_resources.git` # If not previously done. 
    Step 2:$ `sudo tar -xvf out1.tar && tar -xvf resources.tar` # to extract on the same directory

## Uploading FHIR resource bundles and updated value sets to FHIR server
To upload the sample fhir data, you can use tag-uploader tool(see more at https://github.com/smart-on-fhir/tag-uploader)

### Installing/configuring tag-uploader
    Step 1: $ `sudo git clone https://github.com/smart-on-fhir/tag-uploader.git` 
    Step 2: $ `cd tag-uploader`
    Step 3: $ `npm` 

### Uploading FHIR resources
You can read the README text to know how to upload the data to server. In short, you can run this cmd by replacing the folder path and server url (run from inside of `tag-uploader` folder).

$ `node . -d out1/ -S <hostname>:8080/baseDstu3`
 
Important note: for this, you also need to install nodejs >=10. Use the following command for that: 

$ `curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -`
$ `sudo apt-get install -y nodejs`

Then execute the following command to upload FHIR resources (i.e. data)): 

$ `node . -d out1/ -S http://menzel.informatik.rwth-aachen.de:8080/baseDstu3` # replace with your own hostname/server URls

Then execute the following command to upload FHIR resources (i.e. updated valuesets): 

$ `node . -d resources/ -S http://menzel.informatik.rwth-aachen.de:8080/baseDstu3` # replace with your own hostname/server URls

However, this might show an issue with 'colors' package, which subsequetly can be installed using 'npm install colors', which again has dependency with the following:

$ `sudo npm audit fix --force`
$ `sudo npm install colors`

## Using FHIR resources
https://bitbucket.org/rezacsedu/sample_fhir_resources/src/master/Querying_FHIR_server_with_CQL_expression_and_compute_BMI.ipynb can be executed to run the PHT BMI counter. This will:

    Queries a FHIR server (containing 65 patients records in FHIR standard). For this, we use our own FHIR server hosted at http://menzel.informatik.rwth-aachen.de:8080/baseDstu3/. This works both as a FHIR server as well as terminology server.
    Pulls the FHIR resource bundle,
    Performs some minor preprocessing and
    Finally compute the BMI of each patient that satisfy filtering criteria in the CQL query file (see Input.cql file).

If you don't know how to run a Python notebook, you can follow below steps:

    Step 1: Install Jupyter notebook on Python 3 by issuing $`sudo apt-get update && sudo apt-get -y install python2.7 python3-pip python-dev && sudo apt-get -y install ipython ipython-notebook $$ sudo -H pip3 install jupyter`
    Step 2: Then go to terminal and type `jupyter notebook`
    Step 3: above command will take you to the browser
    Step 4: Upload the .ipynb and Input.cql files 
    Step 5: Click on the .ipynb file and run each paragraph sequentially to see the result. 

However, if you want to try at your own FHIR server and CQL evaluation engine, just change the following lines and point to yours:

    cqlEngineURL = "http://menzel.informatik.rwth-aachen.de:8082/cql/evaluate"
    dataServiceUri = "http://menzel.informatik.rwth-aachen.de:8080/baseDstu3/" 
    terminologyServiceUri = "http://menzel.informatik.rwth-aachen.de:8080/baseDstu3/"